package com.example.demo.metier;
import com.example.demo.repository.PersonneRepository;
import com.example.demo.model.Personne;
import org.springframework.beans.factory.annotation.Autowired;

public class TraitementIMC {
    @Autowired
    private PersonneRepository personneRepository;
    public int getImc(int id){
       Personne imcPersonne=  personneRepository.getReferenceById(id);
        double taille = imcPersonne.getTaille();
        int poids = imcPersonne.getPoids();
        double imc = poids / taille * taille;
        return (int) imc;
    }
}
