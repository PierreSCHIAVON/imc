package com.example.demo.controller;

import com.example.demo.model.Personne;
import com.example.demo.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.metier.TraitementIMC;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/personne")
public class PersonneController {

    @Autowired
    private PersonneRepository personneRepository;

    @PostMapping
    public void addPersonne( @RequestBody Personne personne) {
        personneRepository.save(personne);
    }

    @GetMapping
    public List<Personne> personneList(){
        return personneRepository.findAll();
    }

    @GetMapping("/{id}")
    public Personne getPersonne(@PathVariable ("id") Integer id) {
        return  personneRepository.getById(id);
    }

    @PutMapping
    public void updatePersonne(@RequestBody Personne personne) {
        personneRepository.save(personne);
    }

    @DeleteMapping("/{id}")
    public void deletePersonne(@PathVariable ("id") int id) {
        personneRepository.deleteById(id);
    }





        private TraitementIMC traitementIMC;
    @GetMapping("imc/{id}")
    public int getImc(@PathVariable int id){
        return traitementIMC.getImc(id);

    }
    }

